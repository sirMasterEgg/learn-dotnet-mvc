/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./learn-dotnet-mvc/Pages/**/*.cshtml", "./learn-dotnet-mvc/Views/**/*.cshtml"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
};
